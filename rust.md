# Rust and GitLab Duo (AI)

## Ask Duo Chat how to get started

Open the [Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html) prompt, if not already open.

```markdown
Explain how to get started with a new Rust project on the command line
```

```markdown
Show a GitLab CI/CD configuration for Rust to build, test, run
```

## Start writing code

Define a new project.

1. https://about.gitlab.com/blog/2023/08/10/learning-rust-with-a-little-help-from-ai-code-suggestions-getting-started/
1. https://about.gitlab.com/blog/2023/10/12/learn-advanced-rust-programming-with-a-little-help-from-ai-code-suggestions/ 


TODO 

## Learn about source code

Import an existing open source project, and let GitLab Duo explain and refactor the code.