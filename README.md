# Accelerate your development with GitLab and AI - Getting Started Exercises

Livestream, 2024-05-07, 3pm UTC: Accelerate your development with GitLab and AI

[![Accelerate your development with GitLab and AI](https://img.youtube.com/vi/L2Mx8hOhkEE/0.jpg)](https://www.youtube.com/watch?v=L2Mx8hOhkEE)

## Resources

- [GitLab Duo documentation](https://docs.gitlab.com/ee/user/ai_features.html)
- [GitLab Duo Coffee Chat YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp5uj_JgQiSvHw1jQu0mSVZ)

## Requirements for GitLab Duo (AI) exercises

1. IDE: VS Code with GitLab workflow installed
    - [All supported IDEs](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-editor-extensions)
1. [Access to GitLab Duo](https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html#assign-gitlab-duo-pro-seats)

## Preparations

1. Open VS Code and verify that the GitLab workflow extension is enabled, and configured with your GitLab instance (GitLab.com SaaS, Dedicated or Self-managed)
1. Use the Duo Chat icon in the left menu, or the commmand palette (macOS: cmd shift p) and type `gitlab duo chat` to open a new chat prompt.
1. Learn [how to drag&drop the chat view](https://www.youtube.com/watch?v=foZpUvWPRJQ) from the left pane into the right, always open, view.


> Tip: Review the [best practices and tips tutorial for AI-assisted chat prompts and GitLab Duo](https://about.gitlab.com/blog/2024/04/02/10-best-practices-for-using-ai-powered-gitlab-duo-chat/).

## Exercises

- [Python](python.md) - ✅ 
- [Go](go.md) - WIP
- [Rust](rust.md) - WIP

### APIs

Query REST APIs and do something with the result.

- WhatTheCommit API
- Chuck Norris API
- Bacon Ipsum API
- Cat Facts API
